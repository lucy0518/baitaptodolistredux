import { combineReducers } from "redux";
import { todolistReducer } from "./todolistReducer";

export const rootReducer_Todolist = combineReducers({
  todolistReducer,
});
