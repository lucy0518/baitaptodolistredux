import { TodolistDarkTheme } from "../../Themes/TodolistDarkTheme";
import { TodolistLightTheme } from "../../Themes/TodolistLightTheme";
import { TodolistPrimaryTheme } from "../../Themes/TodolistPrimaryTheme";
import { initialTask } from "../../Todolist/utils";
import {
  ADD_TASK,
  CHANGE_INPUT,
  CHOOSE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../constants/constants";
import { nanoid } from "nanoid";

const initialState = {
  themeArr: [
    {
      id: 1,
      name: "Primary Theme",
      theme: TodolistPrimaryTheme,
    },
    {
      id: 2,
      name: "Dark Theme",
      theme: TodolistDarkTheme,
    },
    {
      id: 3,
      name: "Light Theme",
      theme: TodolistLightTheme,
    },
  ],
  currentTheme: TodolistPrimaryTheme,
  taskList: [],
  selectTask: initialTask,
};

export const todolistReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHOOSE_THEME: {
      let theme = state.themeArr.find((theme) => theme.id == payload);

      if (theme) {
        state.currentTheme = theme.theme;
      }

      return { ...state };
    }

    case CHANGE_INPUT: {
      let { value } = payload;
      state.selectTask = { ...state.selectTask, taskName: value };

      return { ...state };
    }

    case ADD_TASK: {
      if (!payload) {
        alert("Task name is required!");
        return { ...state };
      }

      let id = nanoid();
      let dtoTask = {
        id,
        taskName: payload,
        done: false,
      };
      let taskListUpdate = [...state.taskList, dtoTask];
      let index = state.taskList.findIndex((task) => task.taskName === payload);

      if (index !== -1) {
        alert("This task already existed!!");
      } else {
        return { ...state, taskList: taskListUpdate };
      }
    }

    case DONE_TASK: {
      let taskListUpdate = [...state.taskList];
      let index = state.taskList.findIndex((task) => task.id === payload);

      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      return { ...state, taskList: taskListUpdate };
    }

    case DELETE_TASK: {
      let taskListUpdate = [...state.taskList];
      let updatedList = taskListUpdate.filter((task) => task.id !== payload);

      return { ...state, taskList: updatedList };
    }

    case EDIT_TASK: {
      return { ...state, selectTask: payload };
    }

    case UPDATE_TASK: {
      let taskListUpdate = [...state.taskList];
      let updatedList = taskListUpdate.filter((task) => task.id !== payload.id);
      let updateTask = payload;
      updatedList.push(updateTask);

      return { ...state, taskList: updatedList, selectTask: initialTask };
    }

    default:
      return { ...state };
  }
};
