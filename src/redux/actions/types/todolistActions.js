import {
  ADD_TASK,
  CHANGE_INPUT,
  CHOOSE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../../constants/constants";

export const changeInput = (content) => {
  return {
    type: CHANGE_INPUT,
    payload: content,
  };
};

export const addTask = (taskName) => {
  return {
    type: ADD_TASK,
    payload: taskName,
  };
};

export const changeTheme = (themeId) => {
  return {
    type: CHOOSE_THEME,
    payload: themeId,
  };
};

export const doneTask = (taskId) => {
  return {
    type: DONE_TASK,
    payload: taskId,
  };
};

export const deleteTask = (taskId) => {
  return {
    type: DELETE_TASK,
    payload: taskId,
  };
};

export const editTask = (task) => {
  return {
    type: EDIT_TASK,
    payload: task,
  };
};

export const updateTask = (task) => {
  return {
    type: UPDATE_TASK,
    payload: task,
  };
};
