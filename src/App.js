import logo from "./logo.svg";
import "./App.css";
import TodolistLayout from "./Todolist/TodolistLayout";

function App() {
  return (
    <div className="App">
      <TodolistLayout />
    </div>
  );
}

export default App;
