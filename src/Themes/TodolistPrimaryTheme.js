export const TodolistPrimaryTheme = {
  mode: "primary",
  bgColor: "#fff",
  color: "#343a40",
  borderButton: "1px solid #343a40",
  BorderRadiusBtn: "none",
  hoverTextColor: "#fff",
  hoverBgColor: "#343a40",
  borderColor: "#343a40",
};
