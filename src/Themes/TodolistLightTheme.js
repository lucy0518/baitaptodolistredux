export const TodolistLightTheme = {
  mode: "light",
  bgColor: "#fff",
  color: "#7952b3",
  borderButton: "1px solid #7952b3",
  BorderRadiusBtn: "none",
  hoverTextColor: "#fff",
  hoverBgColor: "#7952b3",
  borderColor: "#7952b3",
};
