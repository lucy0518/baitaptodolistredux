import React, { Component } from "react";
import { connect } from "react-redux";
import { ThemeProvider } from "styled-components";
import { Button } from "../Components/Button";
import { Container } from "../Components/Container";
import { Dropdown } from "../Components/Dropdown";
import { Heading1, Heading2, Heading3 } from "../Components/Heading";
import { Table, Th, Thead, Tr } from "../Components/Table";
import { Input, Label, TextField } from "../Components/TextField";
import {
  addTask,
  changeInput,
  changeTheme,
  deleteTask,
  doneTask,
  editTask,
  updateTask,
} from "../redux/actions/types/todolistActions";

class TodolistLayout extends Component {
  state = {
    inputValue: ``,
  };
  renderTheme = () => {
    return this.props.themeArr.map((theme, index) => {
      return (
        <option key={index.toString() + theme.id} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index.toString() + task.id}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(editTask(task));
                }}
                className="border-0 ml-1"
              >
                <i className="fa-solid fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTask(task.id));
                }}
                className="border-0 ml-1"
              >
                <i className="fa-solid fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTask(task.id));
                }}
                className="border-0 ml-1"
              >
                <i className="fa-solid fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskComplete = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index.toString() + task.id}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTask(task.id));
                }}
                className="border-0 ml-1"
              >
                <i className="fa-solid fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  render() {
    let isEdit = !!this.props.selectTask?.id;
    let isAddTask = !this.props.selectTask?.id;

    return (
      <ThemeProvider theme={this.props.currentTheme}>
        <Container className="text-left">
          <Dropdown
            onChange={(e) => {
              this.props.dispatch(changeTheme(e.target.value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading2>To Do List</Heading2>
          <span>
            <Label className="d-block">Task name</Label>
            <Input
              onChange={(e) => {
                this.setState({
                  inputValue: e.target.value,
                });
              }}
              value={this.state.inputValue}
              id="input_task"
              className="w-50"
              type="text"
            />
          </span>
          <Button
            disabled={isEdit}
            onClick={() => {
              this.props.dispatch(addTask(this.state.inputValue));
              this.setState({
                inputValue: "",
              });
            }}
            className="ml-2"
          >
            {" "}
            <i className="fa-solid fa-plus mr-2" />
            Add task
          </Button>
          <Button
            disabled={isAddTask}
            onClick={() => {
              const updatedTask = {
                ...this.props.selectTask,
                taskName: this.state.inputValue,
              };
              this.props.dispatch(updateTask(updatedTask));
              this.setState({
                inputValue: "",
              });
            }}
            className="ml-2"
          >
            {" "}
            <i className="fa-solid fa-upload mr-2"></i>
            Update task
          </Button>
          <hr className="text-white" />

          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>

          <Heading3>Task completed</Heading3>
          <Table>
            <Thead>{this.renderTaskComplete()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.selectTask.id !== this.props.selectTask.id) {
      this.setState({
        inputValue: this.props.selectTask.taskName,
      });
    }
  }
}

let mapStateToProps = (state) => {
  return {
    currentTheme: state.todolistReducer.currentTheme,
    taskList: state.todolistReducer.taskList,
    themeArr: state.todolistReducer.themeArr,
    selectTask: state.todolistReducer.selectTask,
  };
};

export default connect(mapStateToProps)(TodolistLayout);
